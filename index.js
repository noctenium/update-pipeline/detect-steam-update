const SteamUser = require('steam-user');
const MsRest = require('ms-rest-azure');
const { ComputeManagementClient } = require("@azure/arm-compute");

//make sure required files are there

let client = new SteamUser();
const config = require("./config/config.json");

let computeClient = new Promise((resolve, reject)=>{
    MsRest.loginWithServicePrincipalSecret(
        config.AzureAccountInfo.clientId,
        config.AzureAccountInfo.secret,
        config.AzureAccountInfo.domain,
        (err, credentials)=>{
            if(err) reject(err)
            resolve(new ComputeManagementClient(credentials, config.AzureAccountInfo.subscription))
        }
    )
})

//some client options
client.options.enablePicsCache = true;
client.options.changelistUpdateInterval = 60000; //this should be 60 seconds?
client.options.autoRelogin = true;

client.logOn(config.SteamAccountInfo);

client.on('loggedOn', () => {
    console.log(`Logged into Steam as ${client.steamID.getSteam3RenderedID()}`);
})
client.on('ownershipCached', () => {
    if (client.ownsApp(config.TargetAppID)) {
        console.log(`Target app (${config.TargetAppID}) found in library.`)
    } else {
        console.log(`User does not own target app! (${config.TargetAppID})`)
        process.exit(1);
    }
})
client.on('appUpdate', async (appid, productInfo) => {
    if (appid == config.TargetAppID) {
        let client = await computeClient
        console.log("Update Detected!")
        console.log("starting azure instance...")
        client.virtualMachines.start("arborean_group", "arborean");
        console.log("Started VM")
    }
})