FROM node:16-alpine
VOLUME ["/app/config"]
COPY . /app
RUN cd /app && npm install
CMD node /app